Inkscape. Draw Freely.
======================

My fork to the Inkscape project.
UI/UX reimagined.

[![build status](https://gitlab.com/inkscape/inkscape/badges/master/pipeline.svg)](https://gitlab.com/inkscape/inkscape/commits/master)
[![Build Status](https://ci.appveyor.com/api/projects/status/gitlab/inkscape/inkscape?branch=master&svg=true)](https://ci.appveyor.com/project/inkscape/inkscape)

For installation, please see: [INSTALL.md](INSTALL.md)
